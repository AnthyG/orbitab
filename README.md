# Orbitab

- [Firefox Add-on Page](https://addons.mozilla.org/en-GB/firefox/addon/orbitab/)

## Summary

Send those tabs into the discarded orbit!

## Description

Discard one or multiple tabs into a collection ("orbit") by selecting the tabs in the tabbar and clicking the orbitab icon in the toolbar.
Orbits may be renamed and get a colour assigned, deleted and the tabs they contain can be restored with the state they where in before discarding them (e.g. pinned, reader mode).
You can also either search through all orbit's tabs or those of a specific orbit.
