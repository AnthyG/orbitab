const defaultSettings = {
	darkmode: false,
	sortBy: 1,
	sortByDir: 1,
	sortTabsBy: 0,
	sortTabsByDir: 1,
	viewMode: 0
};

const getSettings = async () =>
	Object.fromEntries(
		Object.entries(
			Object.assign(
				{},
				defaultSettings,
				(await browser.storage.local.get("settings")).settings || {}
			)
		).map(s => [
			s[0],
			typeof s[1] !== "undefined" ? s[1] : defaultSettings[s[0]]
		])
	);

const setSettings = async nsettings => {
	Object.assign({}, await getSettings(), nsettings);

	await browser.storage.local.set({ settings: nsettings });

	return Promise.resolve();
};

const setSetting = async (key, value) => {
	await setSettings({
		...(await getSettings()),
		[key]: value
	});

	return Promise.resolve();
};

function handleClick() {
	discardHighlighted();
}

let orbits = [];

const setOrbits = async (nOrbits = orbits) => {
	orbits = nOrbits;
	browser.storage.local.set({ orbits });
};

const colourNames = [
	"amber",
	"blue",
	"blueGrey",
	"brown",
	"cyan",
	"deepOrange",
	"deepPurple",
	"green",
	"grey",
	"indigo",
	"lightBlue",
	"lightGreen",
	"lime",
	"orange",
	"pink",
	"purple",
	"red",
	"teal",
	"yellow"
];
const colourVariants = [
	"200",
	"300",
	"400",
	"500",
	"600",
	"700",
	"800",
	"900",
	"A200",
	"A400",
	"A700"
];
const randColours = Object.entries(materialColors)
	.filter(_ => colourNames.includes(_[0]))
	.map(_ =>
		Object.entries(_[1])
			.filter(__ => colourVariants.includes(__[0]))
			.map(__ => __[1])
			.flat()
	)
	.flat();

const filterTabProps = [
	"active",
	"pinned",
	"hidden",
	"discarded",
	"incognito",
	"isInReaderMode",
	"url",
	"title",
	"favIconUrl"
];
const restoreTabProps = [
	"active",
	"discarded",
	"openInReaderMode",
	"pinned",
	"title",
	"url"
];
const restoreTabPropResolvers = {
	openInReaderMode: tab => tab.isInReaderMode,
	title: tab => (tab.discarded ? tab.title : null),
	url: tab =>
		tab.isInReaderMode
			? decodeURIComponent(tab.url.substr("about:reader?url=".length))
			: tab.url
};

const discardHighlighted = async (
	f = (tab, tabIndex, tabsArr) => {
		return true;
	}
) => {
	const curWindow = await browser.windows.getCurrent();
	const tabs = (
		await browser.tabs.query({
			highlighted: true,
			windowId: curWindow.id
		})
	).filter(f);

	try {
		let uuid = null;
		let t = 0;
		const tMax = 5;
		while (t < tMax) {
			uuid = faker.helpers.slugify(faker.lorem.words(4));

			if (orbits.findIndex(orbit => orbit.uuid === uuid) === -1) {
				break;
			}

			t++;
		}

		let title = uuid;

		if (t === tMax) {
			// uuid = Math.random()
			// 	.toString()
			// 	.substr(2);
			uuid = faker.random.uuid();
			title = `Orbit ${uuid}`;
		}

		const dTab = await browser.tabs.create({
			// title,
			// discarded: true,
			url: `orbits/index.html?uuid=${uuid}`
		});

		console.debug({ dTab });

		const timestamp = moment.now();

		setOrbits([
			...orbits,
			{
				uuid,
				title,
				created: timestamp,
				updated: timestamp,
				colour: faker.random.arrayElement(randColours),
				tabs: tabs.map(tab => {
					return {
						...Object.fromEntries(
							filterTabProps.map(k => [k, tab[k]])
						),
						created: moment.now(),
						updated: moment.now(),
						_id: faker.random.uuid()
					};
				})
			}
		]);

		browser.tabs.remove(tabs.map(tab => tab.id)); // or .discard

		browser.runtime.sendMessage("reloadOrbits");
	} catch (err) {
		console.error({ err });
	}
};

browser.runtime.onMessage.addListener(request => {
	const { type, uuid } = request;
	console.debug({ ...request });
	if (uuid) {
		if (type === "load") {
			let orbit = orbits.find(orbit => orbit.uuid === uuid);
			if (!orbit) {
				return Promise.reject("No tabs for UUID", uuid);
			}

			return Promise.resolve({ orbit });
		} else if (type === "update" && request.orbit) {
			let orbitIndex = orbits.findIndex(orbit => orbit.uuid === uuid);
			if (orbitIndex === -1) {
				return Promise.reject("No tabs for UUID", uuid);
			}

			orbits[orbitIndex] = {
				...orbits[orbitIndex],
				...request.orbit,
				uuid,
				updated: moment.now()
			};

			setOrbits();

			browser.runtime.sendMessage("reloadOrbits");
			browser.runtime.sendMessage([
				"reloadOrbit",
				uuid,
				orbits[orbitIndex]
			]);

			return Promise.resolve();
		} else if (type === "delete") {
			let orbitIndex = orbits.findIndex(orbit => orbit.uuid === uuid);
			if (orbitIndex === -1) {
				return Promise.reject("No tabs for UUID", uuid);
			}

			orbits.splice(orbitIndex, 1);
			setOrbits();

			browser.runtime.sendMessage("reloadOrbits");

			return Promise.resolve();
		} else if (type === "undelete" && request.orbit) {
			let orbit = orbits.find(orbit => orbit.uuid === uuid);
			if (orbit) {
				return Promise.reject("Tabs for UUID already exist");
			}

			setOrbits([...orbits, { ...request.orbit, uuid }]);

			browser.runtime.sendMessage("reloadOrbits");

			return Promise.resolve();
		} else if (type === "restore" && typeof request.dTabId === "number") {
			let orbit = orbits.find(orbit => orbit.uuid === uuid);
			if (!orbit) {
				return Promise.reject("No tabs for UUID", uuid);
			}

			const dTabId = request.dTabId;

			const { tabs } = orbit;

			return new Promise((resolve, reject) => {
				const spawnTabs = () => {
					let spawnedTabs = 0;
					return new Promise((resolve, reject) => {
						tabs.forEach(tab =>
							browser.tabs
								.create({
									...Object.fromEntries(
										restoreTabProps.map(k => [
											k,
											restoreTabPropResolvers.hasOwnProperty(
												k
											)
												? restoreTabPropResolvers[k](
														tab
												  )
												: tab[k]
										])
									),
									openerTabId: dTabId
								})
								.then(() => {
									spawnedTabs++;
									if (spawnedTabs >= tabs.length) {
										resolve();
									}
								}, reject)
						);
					});
				};

				spawnTabs().then(() => {
					browser.tabs.update(dTabId, { active: true });
					resolve();
				}, reject);
			});
		}
		return;
	} else if (type === "orbits") {
		return Promise.resolve(orbits);
	}
	return;
});

(async () => {
	setSettings(await getSettings());

	orbits = (await browser.storage.local.get("orbits")).orbits || [];

	//<Upgrade orbits>
	// Convert to array
	if (orbits && typeof orbits === "object" && !Array.isArray(orbits)) {
		setOrbits([...Object.entries(orbits).map(o => o[1])]);
	}
	// Add injected properties
	setOrbits(
		orbits.map(orbit => {
			const uuid = faker.random.uuid();

			return {
				...Object.assign(
					{
						uuid,
						title: `Orbit ${uuid}`,
						created: moment.now(),
						updated: moment.now(),
						colour: faker.internet.color()
					},
					orbit
				),
				tabs: orbit.tabs.map(tab => {
					return {
						...Object.assign(
							{
								created: moment.now(),
								updated: moment.now(),
								_id: faker.random.uuid()
							},
							tab
						)
					};
				})
			};
		})
	);
	//</Upgrade orbits>

	browser.browserAction.onClicked.addListener(handleClick);
	// browser.browserAction.onClicked.addListener(() =>
	// 	browser.tabs.create({
	// 		url: `${window.location.origin}/orbits/index.html`
	// 	})
	// );

	browser.menus.create({
		contexts: ["browser_action"],
		onclick: handleClick,
		title: "Send highlighted tabs into a new orbit"
	});

	browser.menus.create({
		contexts: ["browser_action"],
		onclick() {
			browser.tabs.create({
				url: `${window.location.origin}/orbits/index.html`
			});
		},
		title: "View all orbits"
	});
})();
