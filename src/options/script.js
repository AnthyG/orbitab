const defaultSettings = {
	darkmode: false,
	sortBy: 1,
	sortByDir: 1,
	sortTabsBy: 0,
	sortTabsByDir: 1,
	viewMode: 0
};

const getSettings = async () =>
	Object.fromEntries(
		Object.entries(
			Object.assign(
				{},
				defaultSettings,
				(await browser.storage.local.get("settings")).settings || {}
			)
		).map(s => [
			s[0],
			typeof s[1] !== "undefined" ? s[1] : defaultSettings[s[0]]
		])
	);

const setSettings = async nsettings => {
	Object.assign({}, await getSettings(), nsettings);

	await browser.storage.local.set({ settings: nsettings });

	return Promise.resolve();
};

const setSetting = async (key, value) => {
	await setSettings({
		...(await getSettings()),
		[key]: value
	});

	return Promise.resolve();
};

$darkmode = document.getElementById("darkmode");

$darkmode.addEventListener("change", async () => {
	await setSetting("darkmode", $darkmode.checked);

	document.documentElement.classList.toggle("darkmode", $darkmode.checked);
	document.documentElement.classList.toggle("lightmode", !$darkmode.checked);
});

(async () => {
	if (!(await browser.storage.local.get("settings")).settings) {
		await browser.storage.local.set({
			settings: {
				darkmode: false,
				sortBy: 1,
				sortByDir: 1,
				sortTabsBy: 0,
				sortTabsByDir: 1,
				viewMode: 0
			}
		});
	}
	const settings =
		(await browser.storage.local.get("settings")).settings || {};

	$darkmode.checked = settings.darkmode;

	document.documentElement.classList.toggle("darkmode", $darkmode.checked);
	document.documentElement.classList.toggle("lightmode", !$darkmode.checked);
})();
