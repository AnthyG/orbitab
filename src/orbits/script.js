const defaultSettings = {
	darkmode: false,
	sortBy: 1,
	sortByDir: 1,
	sortTabsBy: 0,
	sortTabsByDir: 1,
	viewMode: 0
};

const getSettings = async () =>
	Object.fromEntries(
		Object.entries(
			Object.assign(
				{},
				defaultSettings,
				(await browser.storage.local.get("settings")).settings || {}
			)
		).map(s => [
			s[0],
			typeof s[1] !== "undefined" ? s[1] : defaultSettings[s[0]]
		])
	);

const setSettings = async nsettings => {
	Object.assign({}, await getSettings(), nsettings);

	await browser.storage.local.set({ settings: nsettings });

	return Promise.resolve();
};

const setSetting = async (key, value) => {
	await setSettings({
		...(await getSettings()),
		[key]: value
	});

	return Promise.resolve();
};

function getQueryVariable(variable) {
	const query = window.location.search.substring(1);
	const vars = query.split("&");
	for (let i = 0; i < vars.length; i++) {
		const pair = vars[i].split("=");
		if (decodeURIComponent(pair[0]) == variable) {
			return decodeURIComponent(pair[1]);
		}
	}
	return null;
}

/*
FROM https://medium.com/better-programming/generate-contrasting-text-for-your-random-background-color-ac302dc87b4
*/
function rgbToYIQ({ r, g, b }) {
	return (r * 299 + g * 587 + b * 114) / 1000;
}
function hexToRgb(hex) {
	if (!hex || hex === undefined || hex === "") {
		return undefined;
	}

	const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

	return result
		? {
				r: parseInt(result[1], 16),
				g: parseInt(result[2], 16),
				b: parseInt(result[3], 16)
		  }
		: undefined;
}

function contrast(colorHex, threshold = 128) {
	if (colorHex === undefined) {
		return "#000";
	}

	const rgb = hexToRgb(colorHex);

	if (rgb === undefined) {
		return "#000";
	}

	return rgbToYIQ(rgb) >= threshold ? "#000" : "#fff";
}

/*
FROM https://stackoverflow.com/a/54070620/5712160
*/
function rgb2hsv(r, g, b) {
	let v = Math.max(r, g, b),
		n = v - Math.min(r, g, b);
	let h =
		n &&
		(v == r ? (g - b) / n : v == g ? 2 + (b - r) / n : 4 + (r - g) / n);
	return [60 * (h < 0 ? h + 6 : h), v && n / v, v];
}

/*
FROM https://stackoverflow.com/a/22692625/5712160
*/
function hexColorDelta(hex1, hex2) {
	// get red/green/blue int values of hex1
	const r1 = parseInt(hex1.substring(0 + 1, 2 + 1), 16);
	const g1 = parseInt(hex1.substring(2 + 1, 4 + 1), 16);
	const b1 = parseInt(hex1.substring(4 + 1, 6 + 1), 16);
	// get red/green/blue int values of hex2
	const r2 = parseInt(hex2.substring(0 + 1, 2 + 1), 16);
	const g2 = parseInt(hex2.substring(2 + 1, 4 + 1), 16);
	const b2 = parseInt(hex2.substring(4 + 1, 6 + 1), 16);
	// calculate differences between reds, greens and blues
	let r = 255 - Math.abs(r1 - r2);
	let g = 255 - Math.abs(g1 - g2);
	let b = 255 - Math.abs(b1 - b2);
	// limit differences between 0 and 1
	r /= 255;
	g /= 255;
	b /= 255;
	// 0 means opposit colors, 1 means same colors
	return (r + g + b) / 3;
}

(async () => {
	const settings = await getSettings();

	document.documentElement.classList.toggle("darkmode", settings.darkmode);
	document.documentElement.classList.toggle("lightmode", !settings.darkmode);

	const uuid = getQueryVariable("uuid");

	if (uuid) {
		console.debug("Requesting tabs", uuid);
		try {
			let { orbit } = await browser.runtime.sendMessage({
				type: "load",
				uuid
			});
			let { title, colour } = orbit;
			let tabs = orbit.tabs.map((t, _index) => {
				return { ...t, _index };
			});

			console.debug({ uuid, tabs });

			$orbitName = document.getElementById("orbit-name");
			const setTitle = (_title = title) => {
				title = _title;

				orbit.title = title;

				document.getElementById("title").innerText = title;
				document.title = title;
				$orbitName.value = title;
			};
			setTitle();

			$orbitName.addEventListener("change", async () => {
				setTitle($orbitName.value);

				browser.runtime.sendMessage({
					type: "update",
					uuid,
					orbit: { title }
				});
			});

			document.getElementById("view-orbit").style.display = "";

			const momentDateFormat = (m1, m2) =>
				m1.isSame(m2, "year") ? "MMM Do" : "MMM Do, Y";
			const momentTimeFormat = "HH:mm:ss";
			const curTimestamp = moment();
			const curDatestamp = moment(curTimestamp.format("D-M-Y"), "D-M-Y");
			const curYearstamp = moment(curDatestamp.format("Y"), "Y");

			const createdMoment = moment(orbit.created, "x");
			const createdDate = createdMoment.format(
				momentDateFormat(createdMoment, curYearstamp)
			);
			const createdTime = createdMoment.format(momentTimeFormat);
			const createdText =
				"Created: " +
				(createdMoment.isBefore(curDatestamp)
					? createdDate
					: createdTime);
			document.getElementById(
				"orbit-created-stamp"
			).innerText = createdText;

			const $updated = document.getElementById("orbit-updated-stamp");
			let updatedMoment, updatedDate, updatedTime, updatedText;
			setUpdatedText = () => {
				updatedMoment = moment(orbit.updated, "x");
				updatedDate = updatedMoment.format(
					momentDateFormat(updatedMoment, curYearstamp)
				);
				updatedTime = updatedMoment.format(momentTimeFormat);
				updatedText = updatedMoment.isSame(createdMoment)
					? createdText
					: "Updated: " +
					  (updatedMoment.isBefore(curDatestamp)
							? updatedDate
							: updatedTime);
				$updated.innerText = updatedText;
			};
			setUpdatedText();

			const $tabList = document.getElementById("tab-list");
			const tabCount = tabs.length;
			if (tabCount > 0) {
				$tabList.style.display = "";
			}

			const $orbitTabCount = document.getElementById("orbit-tab-count");
			$orbitTabCount.innerText = `${tabCount} tab${
				tabCount === 1 ? "" : "s"
			}`;

			tabs.forEach(tab => {
				const url = tab.isInReaderMode
					? decodeURIComponent(
							tab.url.substr("about:reader?url=".length)
					  )
					: tab.url;

				const $li = document.createElement("li");
				$li.classList.add("item");
				$li.setAttribute("tab-_id", tab._id);

				const $a = document.createElement("a");
				$a.classList.add("inner");
				$a.setAttribute("href", url);
				$a.setAttribute("title", tab.title || tab.url);
				$a.setAttribute("target", "_blank");
				$a.setAttribute("ref", "noopener");

				if (tab.favIconUrl) {
					const $img = document.createElement("img");
					$img.classList.add("favicon");
					$img.setAttribute("src", tab.favIconUrl);
					$img.setAttribute("width", "16");
					$img.setAttribute("height", "16");
					$a.appendChild($img);
				}

				const $text = document.createElement("div");
				$text.classList.add("text");

				const $title = document.createElement("span");
				$title.classList.add("title");
				$title.innerText = tab.title;
				$text.appendChild($title);

				const $url = document.createElement("span");
				$url.classList.add("url");
				$url.innerText = url;
				$text.appendChild($url);

				$a.appendChild($text);
				$li.appendChild($a);
				$tabList.appendChild($li);
			});

			$delete = document.getElementById("delete");
			$undelete = document.getElementById("undelete");
			$restore = document.getElementById("restore");

			$delete
				.getElementsByClassName("btn")[0]
				.addEventListener("click", async () => {
					console.debug("delete");

					$delete.style.display = "none";
					$restore.style.display = "none";
					$undelete.style.display = "";

					browser.runtime.sendMessage({ type: "delete", uuid });
				});
			$undelete
				.getElementsByClassName("btn")[0]
				.addEventListener("click", async () => {
					console.debug("undelete");
					await browser.runtime.sendMessage({
						type: "undelete",
						uuid,
						orbit: {
							...orbit,
							title,
							colour,
							tabs: [...tabs]
								.sort((t1, t2) => t1._index - t2._index)
								.map(tab => {
									delete tab._index;
									return tab;
								})
						}
					});

					$undelete.style.display = "none";
					$restore.style.display = "";
					$delete.style.display = "";
				});

			$restore
				.getElementsByClassName("btn")[0]
				.addEventListener("click", async () => {
					console.debug("restore");
					browser.runtime.sendMessage({
						type: "restore",
						uuid,
						dTabId: (await browser.tabs.getCurrent()).id
					});
				});

			$orbitColourBtn = document.getElementById("orbit-colour-btn");
			$orbitColour = document.getElementById("orbit-colour");

			const setColouredElements = (clr = colour) => {
				const rgb = hexToRgb(clr) || { r: 0, g: 0, b: 0 };
				const rgbT = `${rgb.r}, ${rgb.g}, ${rgb.b}`;
				// $orbitName.style.backgroundColor = `rgb(${rgbT})`;
				// $orbitColourBtn.style.backgroundColor = `rgb(${rgbT}, 0.5)`;
				document.getElementById(
					"dynamic-style"
				).innerText = `html .funky-btn#orbit-colour-btn > .gradient::before,
				html .funky-btn#orbit-colour-btn .btn {
					background-image: linear-gradient(to right top, rgb(${rgbT}), rgba(${rgbT}, 0.75));
				}`
					.split("\n")
					.join("");
				// $orbitName.style.borderColor = `rgb(${rgbT})`;
				$orbitColour.value = clr;
				// $orbitName.style.color = contrast(clr);
				// $orbitName.style.borderColor = contrast(clr);
			};
			setColouredElements();

			$orbitColour.addEventListener("input", () => {
				setColouredElements($orbitColour.value);
			});
			$orbitColour.addEventListener("change", () => {
				colour = $orbitColour.value;

				setColouredElements();

				browser.runtime.sendMessage({
					type: "update",
					uuid,
					orbit: { colour }
				});
			});
			$orbitColourBtn.addEventListener("click", () =>
				$orbitColour.click()
			);

			$switchViewMode = document.getElementById("switch-view-mode");

			let viewMode = settings.viewMode;

			const viewModeTexts = [
				"Titles only",
				"URLs only",
				"Titles and URLs"
			];
			const viewModeTitles = [
				"Show only URLs",
				"Show Titles and URLs",
				"Show only Titles"
			];

			$switchViewMode.getElementsByClassName("btn")[0].innerText =
				viewModeTexts[viewMode];
			$switchViewMode
				.getElementsByClassName("btn")[0]
				.setAttribute("title", viewModeTitles[viewMode]);
			$tabList.setAttribute("view-mode", viewMode);
			$switchViewMode
				.getElementsByClassName("btn")[0]
				.addEventListener("click", async () => {
					viewMode = (viewMode + 1) % 3;

					setSetting("viewMode", viewMode);
					$tabList.setAttribute("view-mode", viewMode);
					$switchViewMode.getElementsByClassName("btn")[0].innerText =
						viewModeTexts[viewMode];
					$switchViewMode
						.getElementsByClassName("btn")[0]
						.setAttribute("title", viewModeTitles[viewMode]);
				});

			const $orbitSearch = document.getElementById("orbit-search");
			const $orbitSearchBtn = document.getElementById("orbit-search-btn");

			const toBeSearchedProps = ["title", "url"];

			const searchOrbit = async () => {
				const searchVal = $orbitSearch.value.toLowerCase();

				$tabList.classList.toggle("search-active", searchVal);

				if (searchVal) {
					// const search = new RegExp(searchVal, "gmi");
					const search = { searchVal };
					search.test = function(str) {
						return str.toLowerCase().includes(this.searchVal);
					};

					let _tabCount = 0;

					tabs.forEach(tab => {
						const inSearch = toBeSearchedProps.some(k =>
							search.test(tab[k])
						);

						_tabCount += inSearch;

						$tabList
							.querySelector(`li[tab-_id="${tab._id}"]`)
							.classList.toggle("in-search", inSearch);
					});

					$orbitTabCount.innerText = `${_tabCount} tab${
						_tabCount === 1 ? "" : "s"
					} / ${tabCount} tab${tabCount === 1 ? "" : "s"}`;
				} else {
					[...$tabList.children].forEach($el =>
						$el.classList.remove("in-search")
					);

					$orbitTabCount.innerText = `${tabCount} tab${
						tabCount === 1 ? "" : "s"
					}`;
				}
			};
			$orbitSearch.addEventListener("change", () => searchOrbit());
			$orbitSearchBtn.addEventListener("click", () => searchOrbit());
			$orbitSearch.addEventListener(
				"keyup",
				() => tabCount < 2000 && searchOrbit()
			);

			const sorterNames = ["index", "title", "url", "created", "updated"];
			const sorterFuncs = {
				// title: (t1, t2) => t1.title.localeCompare(t2.title),
				title: (t1, t2) => {
					const _1 = t1.title.toLowerCase();
					const _2 = t2.title.toLowerCase();
					return _1 < _2 ? -1 : _1 > _2 ? 1 : 0;
				},
				url: (t1, t2) => {
					const _1 = t1.url.toLowerCase();
					const _2 = t2.url.toLowerCase();
					return _1 < _2 ? -1 : _1 > _2 ? 1 : 0;
				},
				created: (t1, t2) => t1.created - t2.created,
				updated: (t1, t2) => t1.updated - t2.updated
			};

			$tabsSort = document.getElementById("tabs-sort");
			$tabsSortDir = document.getElementById("tabs-sort-dir");

			let sortBy = settings.sortTabsBy;
			let sortByDir = settings.sortTabsByDir;

			const sortTabs = async (
				_sortBy = sortBy,
				_sortByDir = sortByDir
			) => {
				await setSetting("sortTabsBy", _sortBy);
				await setSetting("sortTabsByDir", _sortByDir);

				$tabsSort.setAttribute(
					"title",
					`Sort by ${sorterNames[_sortBy + 1]}`
				);
				$tabsSort.getElementsByClassName("sort-name")[0].innerText =
					sorterNames[_sortBy];

				$tabsSortDir.setAttribute(
					"title",
					`Sort in ${["ascending", "descending"][_sortByDir]} order`
				);
				$tabsSortDir.getElementsByClassName("btn")[0].innerText = [
					"👇",
					"👆"
				][_sortByDir];

				const sortName = sorterNames[_sortBy];

				const sortedTabs = sorterFuncs.hasOwnProperty(sortName)
					? tabs.sort((t1, t2) =>
							sorterFuncs[sortName](
								...[
									[t2, t1],
									[t1, t2]
								][_sortByDir]
							)
					  )
					: tabs.sort((t1, t2) => {
							return [
								t2._index - t1._index,
								t1._index - t2._index
							][_sortByDir];
					  });

				sortedTabs.forEach(tab =>
					$tabList.appendChild(
						$tabList.querySelector(`li[tab-_id="${tab._id}"]`)
					)
				);
			};

			$tabsSort.addEventListener("click", () => {
				sortBy = (sortBy + 1) % sorterNames.length;

				sortTabs();
			});
			$tabsSortDir.addEventListener("click", () => {
				sortByDir = (sortByDir + 1) % 2;

				sortTabs();
			});
			sortTabs();

			browser.runtime.onMessage.addListener(async evt => {
				if (
					Array.isArray(evt) &&
					evt[0] === "reloadOrbit" &&
					evt[1] === uuid
				) {
					const curTab = await browser.tabs.getCurrent();
					const curActiveTabs = await browser.tabs.query({
						active: true,
						currentWindow: true
					});
					if (
						!curActiveTabs.some(
							t =>
								t.windowId === curTab.windowId &&
								t.id === curTab.id
						)
					) {
						window.location.reload();
					} else {
						setTitle(evt[2].title);
						setColouredElements(evt[2].colour);
						orbit.updated = evt[2].updated;
						setUpdatedText();
					}
				}
			});
		} catch (err) {
			console.error({ err });
		}
	} else {
		const orbits = (
			await browser.runtime.sendMessage({ type: "orbits" })
		).map((o, _index) => {
			return { ...o, _index };
		});

		console.debug({ orbits });

		const orbitsCount = orbits.length;
		const orbitsTitle = `${orbitsCount} Orbit${
			orbitsCount === 1 ? "" : "s"
		}`;
		document.getElementById("title").innerText = orbitsTitle;
		document.title = orbitsTitle;
		const $orbitsCount = document.getElementById("orbits-count");
		$orbitsCount.innerText = orbitsTitle;

		const $totalTabCount = document.getElementById("total-tab-count");
		const totalTabCount = orbits.reduce(
			(totalTabs, orbit) => totalTabs + orbit.tabs.length,
			0
		);
		$totalTabCount.innerText = `${totalTabCount} tab${
			totalTabCount === 1 ? "" : "s"
		}`;

		document.getElementById("view-orbits").style.display = "";

		const $orbitList = document.getElementById("orbit-list");
		if (orbits.length > 0) {
			document.getElementById("view-orbits").style.height = "";
			document.getElementById("orbits-top-bar").style.display = "";
			$orbitList.style.display = "";
		} else {
			document.getElementById("no-orbits").style.display = "";
		}

		const momentDateFormat = (m1, m2) =>
			m1.isSame(m2, "year") ? "MMM Do" : "MMM Do, Y";
		const momentTimeFormat = "HH:mm:ss";
		const curTimestamp = moment();
		const curDatestamp = moment(curTimestamp.format("D-M-Y"), "D-M-Y");
		const curYearstamp = moment(curDatestamp.format("Y"), "Y");

		const toBeSearchedProps = ["title", "tabs"];
		const toBeSearchedSubProps = {
			tabs: ["title", "url"]
		};

		let _orbitsCount = orbitsCount;
		let _totalTabCount = totalTabCount;

		const searchOrbits = async (
			searchVal = $orbitsSearch.value.toLowerCase(),
			colourVal = colourToFilter
		) => {
			$orbitList.classList.toggle(
				"search-active",
				searchVal || colourVal !== "#000000"
			);

			let filteredOrbits = orbits;

			if (colourVal !== "#000000") {
				filteredOrbits = orbits.filter(
					orbit => hexColorDelta(colourVal, orbit.colour) > 0.85
				);
			}

			let searchedOrbits = filteredOrbits;

			if (searchVal) {
				// const search = new RegExp(searchVal, "gmi");
				const search = { searchVal };
				search.test = function(str) {
					return str.toLowerCase().includes(this.searchVal);
				};

				searchedOrbits = filteredOrbits.filter(orbit =>
					toBeSearchedProps.some(k =>
						toBeSearchedSubProps.hasOwnProperty(k)
							? Array.isArray(orbit[k])
								? orbit[k].some(sub =>
										toBeSearchedSubProps[k].some(k2 =>
											search.test(sub[k2])
										)
								  )
								: toBeSearchedSubProps[k].some(k2 =>
										search.test(orbit[k][k2])
								  )
							: search.test(orbit[k])
					)
				);
			}

			[...$orbitList.children].forEach($el =>
				$el.classList.remove("in-search")
			);

			let _totalTabCount_ = 0;
			searchedOrbits.forEach(orbit => {
				_totalTabCount_ += orbit.tabs.length;

				$orbitList
					.querySelector(`li[orbit-uuid="${orbit.uuid}"]`)
					.classList.add("in-search");
			});
			_totalTabCount = _totalTabCount_;

			_orbitsCount = searchedOrbits.length;

			if (_orbitsCount < orbitsCount) {
				$orbitsCount.innerText = `${_orbitsCount} Orbit${
					_orbitsCount === 1 ? "" : "s"
				} / ${orbitsCount} Orbit${orbitsCount === 1 ? "" : "s"}`;
			} else {
				$orbitsCount.innerText = `${orbitsCount} Orbit${
					orbitsCount === 1 ? "" : "s"
				}`;
			}
			if (_totalTabCount < totalTabCount) {
				$totalTabCount.innerText = `${_totalTabCount} tab${
					_totalTabCount === 1 ? "" : "s"
				} / ${totalTabCount} tab${totalTabCount === 1 ? "" : "s"}`;
			} else {
				$totalTabCount.innerText = `${totalTabCount} tab${
					totalTabCount === 1 ? "" : "s"
				}`;
			}
		};

		const $orbitsSearchBtn = document.getElementById("orbits-search-btn");
		const $orbitsSearch = document.getElementById("orbits-search");
		const $orbitsSearchColourBtn = document.getElementById(
			"orbits-search-colour-btn"
		);
		const $orbitsSearchColour = document.getElementById(
			"orbits-search-colour"
		);
		let colourToFilter = "#000000";

		const setColouredElements = (clr = colourToFilter) => {
			const rgb = hexToRgb(clr) || { r: 0, g: 0, b: 0 };
			const rgbT = `${rgb.r}, ${rgb.g}, ${rgb.b}`;
			document.getElementById(
				"dynamic-style"
			).innerText = `html .funky-btn#orbits-search-colour-btn > .gradient::before,
			html .funky-btn#orbits-search-colour-btn .btn {
				background-image: linear-gradient(to right top, rgb(${rgbT}), rgba(${rgbT}, 0.75));
			}`
				.split("\n")
				.join("");
			$orbitsSearchColour.value = clr;
		};
		setColouredElements();

		$orbitsSearchColour.addEventListener("input", () => {
			setColouredElements($orbitsSearchColour.value);

			if (_orbitsCount < 2500) {
				colourToFilter = $orbitsSearchColour.value;

				searchOrbits();
			}
		});
		$orbitsSearchColour.addEventListener("change", () => {
			colourToFilter = $orbitsSearchColour.value;

			setColouredElements();

			searchOrbits();
		});
		$orbitsSearchColourBtn.addEventListener("click", () =>
			$orbitsSearchColour.click()
		);

		$orbitsSearch.addEventListener("change", () => searchOrbits());
		$orbitsSearchBtn.addEventListener("click", () => searchOrbits());
		$orbitsSearch.addEventListener(
			"keyup",
			() => _totalTabCount < 1000 && searchOrbits()
		);

		orbits.forEach(orbit => {
			const $li = document.createElement("li");
			$li.classList.add("item");
			$li.setAttribute("orbit-uuid", orbit.uuid);

			const $a = document.createElement("a");
			$a.classList.add("inner");
			$a.setAttribute(
				"href",
				`${window.location.origin}/orbits/index.html?uuid=${orbit.uuid}`
			);

			const $colour = document.createElement("div");
			$colour.classList.add("colour");
			$colour.setAttribute(
				"title",
				`Filter by similar colours to ${orbit.colour}`
			);
			const rgb = hexToRgb(orbit.colour) || { r: 0, g: 0, b: 0 };
			$colour.style.backgroundColor = `rgb(${rgb.r}, ${rgb.g}, ${rgb.b})`;
			// $colour.style.boxShadow = `0 0 0.4rem 0.2rem rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 0.25)`;
			$colour.style.borderColor = `rgb(${rgb.r}, ${rgb.g}, ${rgb.b})`;
			$colour.addEventListener("click", e => {
				e.stopPropagation();
				e.preventDefault();

				colourToFilter = orbit.colour;

				setColouredElements();

				searchOrbits();
			});
			$a.appendChild($colour);

			// const $img = document.createElement("img");
			// $img.classList.add("favicon");
			// $img.setAttribute("src", orbit.tabs[0].favIconUrl);
			// $img.setAttribute("width", "16");
			// $img.setAttribute("height", "16");
			// $a.appendChild($img);

			const $text = document.createElement("div");
			$text.classList.add("text");

			const $title = document.createElement("span");
			$title.classList.add("title");
			$title.innerText = orbit.title;
			$text.appendChild($title);

			// const $url = document.createElement("span");
			// $url.classList.add("url");
			// $url.innerText = orbit.url;
			// $text.appendChild($url);

			const $tabCount = document.createElement("span");
			$tabCount.classList.add("tab-count");
			const tabCount = orbit.tabs.length;
			$tabCount.innerText = `${tabCount} tab${tabCount === 1 ? "" : "s"}`;
			$text.appendChild($tabCount);

			const $created = document.createElement("span");
			$created.classList.add("created");
			const createdMoment = moment(orbit.created, "x");
			const createdDate = createdMoment.format(
				momentDateFormat(createdMoment, curYearstamp)
			);
			const createdTime = createdMoment.format(momentTimeFormat);
			const createdText =
				"Created: " +
				(createdMoment.isBefore(curDatestamp)
					? createdDate
					: createdTime);
			$created.innerText = createdText;
			$text.appendChild($created);

			const $updated = document.createElement("span");
			$updated.classList.add("updated");
			const updatedMoment = moment(orbit.updated, "x");
			const updatedDate = updatedMoment.format(
				momentDateFormat(updatedMoment, curYearstamp)
			);
			const updatedTime = updatedMoment.format(momentTimeFormat);
			const updatedText = updatedMoment.isSame(createdMoment)
				? createdText
				: "Updated: " +
				  (updatedMoment.isBefore(curDatestamp)
						? updatedDate
						: updatedTime);
			$updated.innerText = updatedText;
			$text.appendChild($updated);

			$a.appendChild($text);
			$li.appendChild($a);
			$orbitList.appendChild($li);
		});

		const sorterNames = [
			"index",
			"colour",
			"title",
			"tab-count",
			"created",
			"updated"
		];
		const sorterFuncs = {
			// colour: (o1, o2) =>
			// 	rgbToYIQ(hexToRgb(o1.colour)) - rgbToYIQ(hexToRgb(o2.colour)),
			colour: (o1, o2) => {
				const c1 = hexToRgb(o1.colour);
				const c2 = hexToRgb(o2.colour);

				// const v1 = c1.r + c1.g + c1.b;
				// const v2 = c2.r + c2.g + c2.b;
				// return v1 - v2;

				// const r = c1.r - c2.r;
				// const g = c1.g - c2.g;
				// const b = c1.b - c2.b;
				// const sr = Math.sign(r) === -1 ? true : false;
				// const sg = Math.sign(g) === -1 ? true : false;
				// const sb = Math.sign(b) === -1 ? true : false;
				// // return sr ? r : sg ? g : sb ? b : 0;
				// return !sr && sg && !sb
				// 	? 0
				// 	: sr && !sb
				// 	? -1
				// 	: !sr && sb
				// 	? 1
				// 	: 0;

				/*
				IDEA FROM https://stackoverflow.com/a/26036999/5712160
				*/

				const c1_ = rgb2hsv(c1.r, c1.g, c1.b);
				const c2_ = rgb2hsv(c2.r, c2.g, c2.b);

				// return c1_[0] - c2_[0];
				const b = [10, 0.01, 0.05];
				return (
					((c1_[0] - c2_[0]) * b[0] +
						(c1_[1] - c2_[1]) * b[1] +
						(c1_[2] - c2_[2]) * b[2]) /
					3
				);
			},
			// title: (o1, o2) => o1.title.localeCompare(o2.title),
			title: (o1, o2) => {
				const _1 = o1.title.toLowerCase();
				const _2 = o2.title.toLowerCase();
				return _1 < _2 ? -1 : _1 > _2 ? 1 : 0;
			},
			"tab-count": (o1, o2) => o1.tabs.length - o2.tabs.length,
			created: (o1, o2) => o1.created - o2.created,
			updated: (o1, o2) => o1.updated - o2.updated
		};

		$orbitsSort = document.getElementById("orbits-sort");
		$orbitsSortDir = document.getElementById("orbits-sort-dir");

		let sortBy = settings.sortBy;
		let sortByDir = settings.sortByDir;

		const sortOrbits = async (_sortBy = sortBy, _sortByDir = sortByDir) => {
			await setSetting("sortBy", _sortBy);
			await setSetting("sortByDir", _sortByDir);

			$orbitsSort.setAttribute(
				"title",
				`Sort by ${sorterNames[_sortBy + 1]}`
			);
			$orbitsSort.getElementsByClassName("sort-name")[0].innerText =
				sorterNames[_sortBy];

			$orbitsSortDir.setAttribute(
				"title",
				`Sort in ${["ascending", "descending"][_sortByDir]} order`
			);
			$orbitsSortDir.getElementsByClassName("btn")[0].innerText = [
				"👇",
				"👆"
			][_sortByDir];

			const sortName = sorterNames[_sortBy];

			const sortedOrbits = sorterFuncs.hasOwnProperty(sortName)
				? orbits.sort((o1, o2) =>
						sorterFuncs[sortName](
							...[
								[o2, o1],
								[o1, o2]
							][_sortByDir]
						)
				  )
				: orbits.sort((o1, o2) => {
						return [o2._index - o1._index, o1._index - o2._index][
							_sortByDir
						];
				  });

			sortedOrbits.forEach(orbit =>
				$orbitList.appendChild(
					$orbitList.querySelector(`li[orbit-uuid="${orbit.uuid}"]`)
				)
			);
		};

		$orbitsSort.addEventListener("click", () => {
			sortBy = (sortBy + 1) % sorterNames.length;

			sortOrbits();
		});
		$orbitsSortDir.addEventListener("click", () => {
			sortByDir = (sortByDir + 1) % 2;

			sortOrbits();
		});
		sortOrbits();

		browser.runtime.onMessage.addListener(evt => {
			if (evt === "reloadOrbits") {
				window.location.reload();
			}
		});
	}
})();
